<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerIdfgqsx\appDevDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerIdfgqsx/appDevDebugProjectContainer.php') {
    touch(__DIR__.'/ContainerIdfgqsx.legacy');

    return;
}

if (!\class_exists(appDevDebugProjectContainer::class, false)) {
    \class_alias(\ContainerIdfgqsx\appDevDebugProjectContainer::class, appDevDebugProjectContainer::class, false);
}

return new \ContainerIdfgqsx\appDevDebugProjectContainer(array(
    'container.build_hash' => 'Idfgqsx',
    'container.build_id' => 'dec70378',
    'container.build_time' => 1525254990,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerIdfgqsx');
